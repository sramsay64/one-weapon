package com.openThid.LD28;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;


@SuppressWarnings("javadoc")
public class Dart extends Weapon {
	/**
	 * 6 7 8
	 * 5 . 1
	 * 4 3 2
	 */
	public static HashMap<Integer, String> pics = new HashMap<Integer, String>();
	
	/**
	 * 6 7 8
	 * 5 . 1
	 * 4 3 2
	 */
	public int direction;
	
	public Dart() {
		inPlay = false;
		height = 32;
		width = 32;
		direction = 0;
		pics.put(1, "res/dartE.png");
		pics.put(2, "res/dartSE.png");
		pics.put(3, "res/dartS.png");
		pics.put(4, "res/dartSW.png");
		pics.put(5, "res/dartW.png");
		pics.put(6, "res/dartNW.png");
		pics.put(7, "res/dartN.png");
		pics.put(8, "res/dartNE.png");
		weaponsBarImage = getImage(pics.get(1), this);
		image = getImage(pics.get(1), this);
	}
	

	@Override
	public void use(int mouseX, int mouseY, Player player, Enemy[] enemys) {
//		System.out.println(App.getSimpleDirection(new Point(App.screenX/2, App.screenY/2), new Point(mouseX, mouseY)));
		image = getImage(pics.get(App.getSimpleDirection(new Point(Math.round(player.x), Math.round(player.y)), new Point(mouseX, mouseY))), this);
		for (int i = 0; i < enemys.length; i++) {
			if (enemys[i].getBounds().intersects(new Rectangle(player.x + x, player.y + y, width, height))) {
				enemys[i].kill();
			}
		}
		direction = App.getSimpleDirection(new Point(Math.round(player.x), Math.round(player.y)), new Point(mouseX, mouseY));
		switch (direction) {
			case 1:
				velX = 1;
				velY = 0;
				break;
			case 2:
				velX = 1;
				velY = 1;
				break;
			case 3:
				velX = 0;
				velY = 1;
				break;
			case 4:
				velX = -1;
				velY = 1;
				break;
			case 5:
				velX = -1;
				velY = 0;
				break;
			case 6:
				velX = -1;
				velY = -1;
				break;
			case 7:
				velX = 0;
				velY = -1;
				break;
			case 8:
				velX = 1;
				velY = -1;
				break;
			default:
				System.err.println("error");
		}
		x += player.x;
		y += player.y;
		inPlay = true;
		used = true;
	}

	@Override
	public Image getDisplayedImage(int mouseX, int mouseY, Player player) {
		if (!used) {
			return getImage(pics.get(App.getSimpleDirection(new Point(Math.round(player.x), Math.round(player.y)), new Point(mouseX, mouseY))), this);
		}
		return getImage(pics.get(direction), this);
	}
	
	@Override
	public void update(Player player, Enemy[] enemys) {
		if (inPlay) {
			x += velX;
			y += velY;
			for (int i = 0; i < enemys.length; i++) {
				if (enemys[i].getBounds().intersects(new Rectangle(x, y, width, height))) {
					enemys[i].kill();
				}
			}
		}
	}
}