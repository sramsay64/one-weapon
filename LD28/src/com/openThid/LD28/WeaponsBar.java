package com.openThid.LD28;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.HashMap;


@SuppressWarnings("javadoc")
public class WeaponsBar {
	
	public BufferedImage backBuffer;
	public Graphics2D g2D;
	
	public int y;
	public int x;

	public AffineTransform affineTransform;

	public ArrayList<Weapon> weapons;
	
	@Deprecated
	public HashMap<Weapon, Boolean> weaponUsed;
	
	public int selected = 0;
	
	public WeaponsBar(int width, int height) {
		y = height;
		x = width;
		weaponUsed = new HashMap<Weapon, Boolean>();
		weapons = new ArrayList<Weapon>();
		affineTransform = new AffineTransform();
		backBuffer = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
		g2D = backBuffer.createGraphics();
	}
	
	public void draw(Graphics2D g, ImageObserver observer, int pos) {
		g2D.setTransform(affineTransform);
		g2D.setPaint(Color.DARK_GRAY);
		g2D.fillRect(0, 0, x, y);
		for (int i = 0; i < weapons.size(); i++) {
			int transX = i*64 + 100;
			int transY = 0;
			
			g2D.drawImage(weapons.get(i).weaponsBarImage, transX+16, transY+16, observer);
			g2D.setPaint(Color.WHITE);
			g2D.drawString(i+1 + " " + weapons.get(i).getClass().getSimpleName() + "(" + getAmountLeft(weapons.get(i)) + ")", transX, transY + 10);
		}
		
		g2D.setPaint(Color.WHITE);
		g2D.drawImage(getSelectedWeapon().image, 16, 16, observer);
		
		g2D.setPaint(Color.WHITE);
		g2D.drawRect(15 + selected*64 + 100, 15, 34, 34);
		g2D.drawString(1 + " " + getSelectedWeapon().getClass().getSimpleName() + "(" + getAmountLeft(weapons.get(selected)) + ")", 0, 0 + 10);
		
		g.setTransform(affineTransform);
		g.drawImage(backBuffer, 0, pos, observer);
	}
	
	public int getAmountLeft(Weapon weapon) {
		if (weapon.used) {
			return 0;
		}
		return 1;
	}
	
	public void add(Weapon weapon) {
		weapons.add(weapon);
		weaponUsed.put(weapon, false);
	}
	
	public void setSelected(int i) {
		if (i > weapons.size() || i < 1) {
			return;
		}
		selected = i-1;
	}
	
	public Weapon getSelectedWeapon() {
		return weapons.get(selected);
	}
}