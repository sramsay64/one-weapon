package com.openThid.LD28;

import java.util.HashMap;


/**
 * @author Scott Ramsay
 *
 */
public class Level {
	
	/**
	 * 
	 */
	public static final String[] levelNames = {"Level 1", "Level 2", "Level 3", "Level 4"};
	
	/**
	 * array of enemys
	 */
	public Enemy[] enemys;
	
	/**
	 * player
	 */
	public Player player;
	
	/**
	 * @param player
	 * @param enemys
	 */
	public Level(Player player, Enemy[] enemys) {
		this.player = player;
		this.enemys = enemys;
	}
	
	/**
	 * @param n
	 * @return Example
	 */
	public static Level getExample(int n) {
		Player p = null;
		WeaponsBar wb;
		Enemy[] e;
		Level l;
		switch (n) {
			case 0:
				e = new Enemy[5];
				e[0] = new Enemy(1, 64, 200);
				e[1] = new Enemy(3, 100, 100);
				e[2] = new Enemy(3, 64, 100);
				e[3] = new Enemy(2, 250, 250);
				e[4] = new Enemy(2, 200, 200);
				
				wb = new WeaponsBar(App.screenX, 64);
				wb.add(new Sword());
				wb.add(new Dart());
				p = new Player(wb);
				
				break;
			case 1:
				e = new Enemy[7];
				break;
			case 2:
				e = new Enemy[3];
				break;
			default:
				return null;
		}
		
		l = new Level(p, e);
		
		switch (n) {
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		}
		return l;
	}
	
	/**
	 * @param n
	 * @return HashMap with levels
	 */
	public static HashMap<String, Level> getLevels() {
		HashMap<String, Level> h = new HashMap<String, Level>();
		
		Player p = null;
		WeaponsBar wb = null;
		Enemy[] e = null;
		Level l = null;
		
		/* * * * * * * *
		 *   Level 1   *
		 * * * * * * * */
		e = new Enemy[5];
		e[0] = new Enemy(1, 64, 200);
		e[1] = new Enemy(3, 100, 100);
		e[2] = new Enemy(3, 64, 100);
		e[3] = new Enemy(2, 250, 250);
		e[4] = new Enemy(2, 200, 200);
		
		wb = new WeaponsBar(App.screenX, 64);
		wb.add(new Sword());
		wb.add(new Dart());
		p = new Player(wb);
		
		l = new Level(p, e);
		h.put(levelNames[0], l);
		/////////////////////////////////////
		
		/* * * * * * * *
		 *   Level 2   *
		 * * * * * * * */
		e = new Enemy[7];
		e[0] = new Enemy(1, 64, 200);
		e[1] = new Enemy(1, 64, 232);
		e[2] = new Enemy(1, 100, 100);
		e[3] = new Enemy(1, 64, 100);
		e[4] = new Enemy(0, 250, 250);
		e[5] = new Enemy(0, 200, 200);
		e[6] = new Enemy(0, 300, 300);
		
		wb = new WeaponsBar(App.screenX, 64);
		wb.add(new Sword());
		wb.add(new Dart());
		p = new Player(wb);
		
		l = new Level(p, e);
		h.put(levelNames[1], l);
		/////////////////////////////////////
		
		/* * * * * * * *
		 *   Level 3   *!
		 * * * * * * * */
		e = new Enemy[8];
		e[0] = new Enemy(1, 64, 64);
		e[1] = new Enemy(3, 100, 64);
		e[2] = new Enemy(0, 340, 104);
		e[3] = new Enemy(0, 340, 64);
		e[4] = new Enemy(0, 300, 64);
		e[5] = new Enemy(2, 200, 200);
		e[6] = new Enemy(2, 232, 200);
		e[7] = new Enemy(2, 200, 232);
		
		wb = new WeaponsBar(App.screenX, 64);
		wb.add(new Sword());
		wb.add(new Dart());
		p = new Player(wb);
		
		l = new Level(p, e);
		h.put(levelNames[2], l);
		/////////////////////////////////////
		
		/* * * * * * * *
		 *   Last      *!
		 * * * * * * * */
		e = new Enemy[12];
		e[10]= new Enemy(1, 64, 100);
		e[11]= new Enemy(3, 100, 100);
		e[0] = new Enemy(1, 64, 64);
		e[1] = new Enemy(3, 100, 64);
		e[2] = new Enemy(3, 232, 232);
		e[3] = new Enemy(2, 200, 200);
		e[4] = new Enemy(2, 232, 200);
		e[5] = new Enemy(2, 200, 232);
		e[6] = new Enemy(3, 232, 264);
		e[7] = new Enemy(2, 200, 300);
		e[8] = new Enemy(2, 232, 300);
		e[9] = new Enemy(2, 200, 264);
		
		wb = new WeaponsBar(App.screenX, 64);
		wb.add(new Sword());
		wb.add(new Dart());
		p = new Player(wb);
		
		l = new Level(p, e);
		h.put(levelNames[3], l);

		return h;
	}

	/**
	 * @param levelName
	 * @return Level
	 */
	public static Level getLevel(String levelName) {
		return getLevels().get(levelName);
	}
}