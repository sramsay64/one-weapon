package com.openThid.LD28;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.net.URL;



@SuppressWarnings("javadoc")
public class Entity {
	
	public AffineTransform affineTransform;
	
	public Image image;
	
	public int x = 0;
	public int y = 0;
	public float velX = 0;
	public float velY = 0;
	public int height = 0;
	public int width = 0;
	
	public Entity() {
		affineTransform = new AffineTransform();
	}
	
	public void updateAffineTransform() {
		affineTransform = new AffineTransform();
		affineTransform.translate(x, y);
	}
	
	public void updatePos() {
		x += velX;
		y += velY;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	public Image getImage(String file) {
		URL url = this.getClass().getResource(file);
		Toolkit tk = Toolkit.getDefaultToolkit();
		return tk.getImage(url);
	}
	
	public static Image getImage(String file, Object o) {
		URL url = o.getClass().getResource(file);
		Toolkit tk = Toolkit.getDefaultToolkit();
		return tk.getImage(url);
	}
}