package com.openThid.LD28;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;


@SuppressWarnings("javadoc")
public class App extends Applet implements Runnable, KeyListener, MouseListener, MouseMotionListener{
	
	int[] x = {1, 8, 5};
	int[] y = {7, 0, 4};
	
	public static App app;
	public Window wind = null;
	
	BufferedImage backbuffer;
	Graphics2D g2D;
	Thread thread;
	AffineTransform affineTransformBlank = new AffineTransform();
	
	Player playerMain;
	Enemy[] enemys;
	
	Random random;
	
	public int mouseX;
	public int mouseY;
	
	private int exitCode;
	
	public boolean gameRunning;
	
	@Deprecated
	public Level level;
	
	/**
	 * X Size of Screen
	 */
	public static final int screenX = 1200;
	/**
	 * Y Size of Screen
	 */
	public static final int screenY = 800;
	
	
	public static int fieldX;
	public static int fieldY;
	
	private static final long serialVersionUID = -6314099207948369397L;
	
	public void appStatic() {
		app = this;
	}
	
	public void init() {
		app = this;
		gameRunning = true;
		exitCode = -1;
		
//		openLevel(Level.getLevel(L));
		
//		playerMain = new Player();
//		playerMain.x = 16;
//		playerMain.y = 16;
//		playerMain.x = screenX/2 - playerMain.width/2;
//		playerMain.y = screenY/2 - playerMain.height/2;
		
		fieldX = screenX;
		fieldY = screenY-playerMain.weaponsBar.y;
		
//		random = new Random();

//		enemys = new Enemy[100];
		
//		for (int i = 0; i < enemys.length; i++) {
//			do {
//				enemys[i] = new Enemy(random.nextInt(Enemy.pics.length), random.nextInt(fieldX-Enemy.width), random.nextInt(fieldY-Enemy.height));
//			} while (	(enemys[i].x < 64 && enemys[i].y < 64)
//					||	(false)
//					||	(enemys[i].x < 64 && enemys[i].y < 64)
//					);
//		}

//		playerMain.weaponsBar.add(new Sword());
//		playerMain.weaponsBar.add(new Dart());
		
		backbuffer = new BufferedImage(screenX, screenY, BufferedImage.TYPE_INT_RGB);
		g2D = backbuffer.createGraphics();
		
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public void openLevel(String levelName) {
		Level l = Level.getLevel(levelName);
		playerMain = l.player;
		enemys = l.enemys;
	}
	
	public void openLevel(Level l) {
		playerMain = l.player;
		enemys = l.enemys;
	}
	
	public void gameOver(boolean win) {
		gameRunning = false;
		if (win) {
			exitCode = 1;
			if (wind != null) {
				wind.postExitCode(exitCode);
			}
			System.out.println("0-----------0");
			System.out.println("|    Win    |");
			System.out.println("0-----------0");
		} else {
			exitCode = 0;
			if (wind != null) {
				wind.postExitCode(exitCode);
			}
			System.out.println("0-----------0");
			System.out.println("| Game Over |");
			System.out.println("0-----------0");
		}
	}
	
	/**
	 * -1	=	game still running
	 * 0	=	lose
	 * 1	=	win
	 * @return exitCode
	 */
	public int getExitCode() {
		return exitCode;
	}

	@Override
	public void run() {
		Thread t = Thread.currentThread();
		while (t == thread) {
			try {
				gameUpdate();
				Thread.sleep(20);
			}
			catch(InterruptedException e) {
				e.printStackTrace();
			}
			repaint();
		}
	}
	
	private void gameUpdate() {
		if (gameRunning) {
			playerMain.updatePos();
			playerMain.weaponsBar.getSelectedWeapon().update(playerMain, enemys);
//			System.out.println("X	" + playerMain.weaponsBar.getSelectedWeapon().x + "	Y	" + playerMain.weaponsBar.getSelectedWeapon().y + "	velX	" + playerMain.weaponsBar.getSelectedWeapon().velX + "	velY	" + playerMain.weaponsBar.getSelectedWeapon().velY);
			boolean aliveEnemy = false;
			for (int i = 0; i < enemys.length; i++) {
				if (enemys[i].getBounds().intersects(playerMain.getBounds()) && enemys[i].alive) {
					gameOver(false);
				}
				if (enemys[i].alive) {
					aliveEnemy = true;
				}
			}
			if (!aliveEnemy) {
				gameOver(true);
			}
		}
	}
	
	public void keyTyped(KeyEvent k) {
		
	}
	
	public void keyPressed(KeyEvent k) {
		playerMain.directionY = 0;
		playerMain.directionX = 0;
		
		switch (k.getKeyCode()) {
			case KeyEvent.VK_A:
				playerMain.directionX = -1;
				break;
			case KeyEvent.VK_D:
				playerMain.directionX = 1;
				break;
			case KeyEvent.VK_W:
				playerMain.directionY = -1;
				break;
			case KeyEvent.VK_S:
				playerMain.directionY = 1;
				break;
			case KeyEvent.VK_Q:
				playerMain.weaponsBar.setSelected(playerMain.weaponsBar.selected);
				break;
			case KeyEvent.VK_E:
				playerMain.weaponsBar.setSelected(playerMain.weaponsBar.selected+2);
				break;
			case KeyEvent.VK_1:
			case KeyEvent.VK_NUMPAD1:
				playerMain.weaponsBar.setSelected(1);
				break;
			case KeyEvent.VK_2:
			case KeyEvent.VK_NUMPAD2:
				playerMain.weaponsBar.setSelected(2);
				break;
			case KeyEvent.VK_3:
			case KeyEvent.VK_NUMPAD3:
				playerMain.weaponsBar.setSelected(3);
				break;
			case KeyEvent.VK_4:
			case KeyEvent.VK_NUMPAD4:
				playerMain.weaponsBar.setSelected(4);
				break;
			case KeyEvent.VK_5:
			case KeyEvent.VK_NUMPAD5:
				playerMain.weaponsBar.setSelected(5);
				break;
			case KeyEvent.VK_6:
			case KeyEvent.VK_NUMPAD6:
				playerMain.weaponsBar.setSelected(6);
				break;
			case KeyEvent.VK_7:
			case KeyEvent.VK_NUMPAD7:
				playerMain.weaponsBar.setSelected(7);
				break;
			case KeyEvent.VK_8:
			case KeyEvent.VK_NUMPAD8:
				playerMain.weaponsBar.setSelected(8);
				break;
			case KeyEvent.VK_9:
			case KeyEvent.VK_NUMPAD9:
				playerMain.weaponsBar.setSelected(9);
				break;
		}
	}
	
	public void keyReleased(KeyEvent k) {
		playerMain.directionY = 0;
		playerMain.directionX = 0;
	}
	
	public void update(Graphics g) {
		g2D.setTransform(affineTransformBlank);
		g2D.setPaint(Color.GRAY);
		g2D.fillRect(0, 0, screenX, screenY);
		
		
		drawEnemys(enemys, g2D);
		drawPlayer(playerMain, g2D);

		g2D.setPaint(Color.WHITE);
//		g2D.drawString("" + getSimpleDirection(new Point((playerMain.x+(playerMain.width/2)), (playerMain.y+(playerMain.height/2))), new Point(mouseX, mouseY)), 5, 10);
		
		paint(g);
	}
	
	private void drawEnemys(Enemy[] e, Graphics2D g) {
		for (int i = 0; i < e.length; i++) {
			e[i].updateAffineTransform();
			g.setTransform(e[i].affineTransform);
			g.drawImage(e[i].getImage(), 0, 0, this);
		}
	}
	
	private void drawWeaponsBar(Graphics2D g) {
		playerMain.weaponsBar.draw(g, this, App.screenY-playerMain.weaponsBar.y);
	}
	
	private void drawPlayer(Player p, Graphics2D g) {
		p.updateAffineTransform();
		g.setTransform(p.affineTransform);
		g.drawImage(p.image, 0, 0, this);
		int gsd = getSimpleDirection(new Point((playerMain.x+(playerMain.width/2)), (playerMain.y+(playerMain.height/2))), new Point(mouseX, mouseY));
		if (!p.weaponsBar.getSelectedWeapon().inPlay) {
			switch (gsd) {
				case 1:
					p.weaponsBar.getSelectedWeapon().x = 32;
					p.weaponsBar.getSelectedWeapon().y = 0;
					break;
				case 2:
					p.weaponsBar.getSelectedWeapon().x = 32;
					p.weaponsBar.getSelectedWeapon().y = 32;
					break;
				case 3:
					p.weaponsBar.getSelectedWeapon().x = 0;
					p.weaponsBar.getSelectedWeapon().y = 32;
					break;
				case 4:
					p.weaponsBar.getSelectedWeapon().x = -32;
					p.weaponsBar.getSelectedWeapon().y = 32;
					break;
				case 5:
					p.weaponsBar.getSelectedWeapon().x = -32;
					p.weaponsBar.getSelectedWeapon().y = 0;
					break;
				case 6:
					p.weaponsBar.getSelectedWeapon().x = -32;
					p.weaponsBar.getSelectedWeapon().y = -32;
					break;
				case 7:
					p.weaponsBar.getSelectedWeapon().x = 0;
					p.weaponsBar.getSelectedWeapon().y = -32;
					break;
				case 8:
					p.weaponsBar.getSelectedWeapon().x = 32;
					p.weaponsBar.getSelectedWeapon().y = -32;
					break;
			}
		}
		if (p.weaponsBar.getSelectedWeapon().inPlay) {
			g.setTransform(affineTransformBlank);
		}
		g.drawImage(p.weaponsBar.getSelectedWeapon().getDisplayedImage(mouseX, mouseY, playerMain), p.weaponsBar.getSelectedWeapon().x, p.weaponsBar.getSelectedWeapon().y, this);
		drawWeaponsBar(g);
	}
	
	
	
	
	
	
	
	public void start() {
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop() {
		thread = null;
	}
	
	public void paint(Graphics g) {
		g.drawImage(backbuffer, 0, 0, this);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if (gameRunning) {
			if (e.getButton() == MouseEvent.BUTTON3) {
				if (!playerMain.weaponsBar.getSelectedWeapon().used) {
					playerMain.use(e.getX(), e.getY(), enemys);
				}
//				System.out.println("playerMain.use(" + e.getX() + ", " + e.getY() + ", " + playerMain.x + ", " + playerMain.y + ", " + enemys + ");");
			} else if (e.getButton() == MouseEvent.BUTTON1) {
				
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
	}
	
	/**
	 * 6 7 8
	 * 5 . 1
	 * 4 3 2
	 * 
	 * . = p1
	 */
	public static int getSimpleDirection(Point p1, Point p2) {
		double gd = getDirection(p2.x - p1.x, p2.y - p1.y);
//		int d = (int) Math.round(gd);
		double ofSet = 45.0/2;
		
		if (gd > 0+ofSet && gd < 45+ofSet) {
//			System.out.println("0-45	" + d);
			return 2;
			
		} else if (gd > 45+ofSet && gd < 90+ofSet) {
//			System.out.println("45-90	" + d);
			return 1;
			
		} else if (gd > 90+ofSet && gd < 135+ofSet) {
//			System.out.println("90-135	" + d);
			return 8;
			
		} else if (gd > 135+ofSet && gd < 180+ofSet) {
//			System.out.println("135-180	" + d);
			return 7;
			
		} else if (gd > 180+ofSet && gd < 225+ofSet) {
//			System.out.println("180-225	" + d);
			return 6;
			
		} else if (gd > 225+ofSet && gd < 270+ofSet) {
//			System.out.println("225-270	" + d);
			return 5;
			
		} else if (gd > 270+ofSet && gd < 315+ofSet) {
//			System.out.println("270-315	" + d);
			return 4;
			
		} else if (gd > 315+ofSet && gd < 360+ofSet) {
//			System.out.println("315-360	" + d);
			return 3;
			
		} else if (gd > -45+ofSet && gd < 0+ofSet) {
//			System.out.println("-45-0	" + d);
			return 3;
			
		}
		return 0;
	}
	/**
	 * @param x
	 * @param y
	 * @return angle in degrees (0 being East)
	 */
	public static double getDirection(double x, double y) {
		double rtn = 0.0;
//		System.out.println(x + "	" + y);
		if (x == 0 || y == 0) {
			if (x == 0) {
				if (y > 0) {
					return 0;
				}
				return 180;
			}
			if (x > 0) {
				return 90;
			}
			return 270;
		} else if (x > 0 && y > 0) {//SE
//			System.out.println("	SE");
			rtn = Math.toDegrees(Math.atan(x/y));
		} else if (x < 0 && y > 0) {//SW
//			System.out.println("	SW");
			rtn = Math.toDegrees(Math.atan(-(-x/y)))+270+90;
		} else if (x > 0 && y < 0) {//NE
//			System.out.println("	NE");
			rtn = Math.toDegrees(Math.atan(-(x/-y)))+180;
		} else {//NW
//			System.out.println("	NW");
			rtn = Math.toDegrees(Math.atan(x/y))+180;
		}
		return rtn;
	}
}