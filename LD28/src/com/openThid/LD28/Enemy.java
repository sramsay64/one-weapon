package com.openThid.LD28;

import java.awt.Image;
import java.awt.Rectangle;

@SuppressWarnings("javadoc")
public class Enemy extends Entity{
	
	public static final int height = 32;
	public static final int width = 32;
	public static final float speed = (float) 0.1;
	public static final String[] pics = {"res/enemy1.png", "res/enemy2.png", "res/enemy3.png", "res/enemy4.png"};
	
	public boolean alive;
	public int type;
	public Image imageDead;
	
	public Enemy(int type) {
		this(type, 0, 0);
	}
	
	public Enemy(int type, int x, int y) {
		this.x = x;
		this.y = y;
		this.type = type;
		alive = true;
		try {
			image = getImage(pics[type]);
		} catch (NullPointerException e) {
			image = getImage(pics[0]);
		}
		
		try {
			imageDead = getImage("res/enemyDead.png");
		} catch (NullPointerException e) {
			imageDead = getImage(pics[0]);
		}
	}

	public void kill() {
		alive = false;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(Math.round(x), Math.round(y), width, height);
	}
	
	public Image getImage() {
		if (alive) {
			return image;
		}
		return imageDead;
	}
}