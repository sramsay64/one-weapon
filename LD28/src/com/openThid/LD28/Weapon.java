package com.openThid.LD28;

import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

@SuppressWarnings("javadoc")
public class Weapon extends Entity{

	public Image image;
	public Image weaponsBarImage;
	public boolean used;
	public boolean inPlay;
	
	public Weapon() {
		
	}
	
	public Image getImage(String file) {
		URL url = this.getClass().getResource(file);
		Toolkit tk = Toolkit.getDefaultToolkit();
		return tk.getImage(url);
	}
	
	public void use(int mouseX, int mouseY, Player player, Enemy[] enemys) {
		used = true;
	}
	
	public Image getDisplayedImage(int mouseX, int mouseY, Player player) {
		return image;
	}
	
	public void update(Player player, Enemy[] enemys) {
		
	}
}