package com.openThid.LD28;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JComboBox;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;

/**
 * @author Scott Ramsay
 *
 */
public class Window {
	
	private JFrame mainFrame;
	private JComboBox comboBox;
	private GameWindow game;
	private Window thiss;
	private JScrollPane scrollPane;
	private JTextPane txtpnHelp;
	private JTextPane textPane;
	private JLabel lblWin;

	/**
	 * Launch the application.
	 * @param args 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.mainFrame.setVisible(true);
//					window.gameFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mainFrame = new JFrame();
		mainFrame.setTitle("One Weapon");
		mainFrame.setBounds(100, 100, 500, 300);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
//		game = new GameWindow();
		thiss = this;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{20, 0, 20, 0, 20, 0};
		gridBagLayout.rowHeights = new int[]{20, 0, 20, 0, 20, 0, 20, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		mainFrame.getContentPane().setLayout(gridBagLayout);
		
		comboBox = new JComboBox(Level.levelNames);
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				System.out.println("" + comboBox.getSelectedItem().toString());
			}
		});
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 3;
		gbc_comboBox.gridy = 1;
		mainFrame.getContentPane().add(comboBox, gbc_comboBox);
		
		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent paramActionEvent) {
//				System.out.println("" + comboBox.getSelectedItem().toString());
				lblWin.setText("");
				game = new GameWindow(comboBox.getSelectedItem().toString(), thiss);
			}
		});
		
		textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setText("W A S D to move" + "\n" + "right click to use weapon" + "\n" + "1-9 or E/Q to change weapons");
		GridBagConstraints gbc_textPane = new GridBagConstraints();
		gbc_textPane.gridheight = 5;
		gbc_textPane.insets = new Insets(0, 0, 5, 5);
		gbc_textPane.fill = GridBagConstraints.BOTH;
		gbc_textPane.gridx = 1;
		gbc_textPane.gridy = 1;
		mainFrame.getContentPane().add(textPane, gbc_textPane);
		
		lblWin = new JLabel("");
		GridBagConstraints gbc_lblWin = new GridBagConstraints();
		gbc_lblWin.insets = new Insets(0, 0, 5, 5);
		gbc_lblWin.gridx = 3;
		gbc_lblWin.gridy = 2;
		mainFrame.getContentPane().add(lblWin, gbc_lblWin);
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 3;
		gbc_scrollPane.gridy = 3;
		mainFrame.getContentPane().add(scrollPane, gbc_scrollPane);
		
		txtpnHelp = new JTextPane();
		txtpnHelp.setEditable(false);
		txtpnHelp.setText("Select a Level and click play \n\n" + "MAKE SURE YOU CLICK INSIDE THE WINDOW!\n\n" + "You only get one of each weapon");
		scrollPane.setViewportView(txtpnHelp);
		GridBagConstraints gbc_btnPlay = new GridBagConstraints();
		gbc_btnPlay.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlay.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnPlay.gridx = 3;
		gbc_btnPlay.gridy = 5;
		mainFrame.getContentPane().add(btnPlay, gbc_btnPlay);
		
		
	}
	
	/**
	 * @param exitCode
	 */
	public void postExitCode(int exitCode) {
		if (exitCode == 1) {
			lblWin.setText("You Win!");
		} else {
			lblWin.setText("You Lose");
		}
		game.gameFrame.dispose();
	}
}
