package com.openThid.LD28;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.HashMap;

import com.openThid.LD28.*;

@SuppressWarnings("javadoc")
public class Sword extends Weapon {
	/**
	 * 6 7 8
	 * 5 . 1
	 * 4 3 2
	 */
	public static HashMap<Integer, String> pics = new HashMap<Integer, String>();
	
	public Sword() {
		inPlay = false;
		height = 32;
		width = 32;
		pics.put(1, "res/swordE.png");
		pics.put(2, "res/swordSE.png");
		pics.put(3, "res/swordS.png");
		pics.put(4, "res/swordSW.png");
		pics.put(5, "res/swordW.png");
		pics.put(6, "res/swordNW.png");
		pics.put(7, "res/swordN.png");
		pics.put(8, "res/swordNE.png");
		weaponsBarImage = getImage(pics.get(6), this);
		image = getImage(pics.get(6), this);
	}
	
	@Override
	public void use(int mouseX, int mouseY, Player player, Enemy[] enemys) {
//		System.out.println(App.getSimpleDirection(new Point(App.screenX/2, App.screenY/2), new Point(mouseX, mouseY)));
		image = getImage(pics.get(App.getSimpleDirection(new Point(Math.round(player.x), Math.round(player.y)), new Point(mouseX, mouseY))), this);
		for (int i = 0; i < enemys.length; i++) {
			if (enemys[i].getBounds().intersects(new Rectangle(player.x + x, player.y + y, width, height))) {
				enemys[i].kill();
			}
		}
		used = true;
	}

	@Override
	public Image getDisplayedImage(int mouseX, int mouseY, Player player) {
		return getImage(pics.get(App.getSimpleDirection(new Point(Math.round(player.x), Math.round(player.y)), new Point(mouseX, mouseY))), this);
	}
	
	@Override
	public void update(Player player, Enemy[] enemys) {
		
	}
}