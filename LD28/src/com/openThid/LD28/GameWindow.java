package com.openThid.LD28;

import java.awt.BorderLayout;

import javax.swing.JFrame;

@SuppressWarnings("javadoc")
public class GameWindow {

	public JFrame gameFrame;
	public App app;
	private String level;
	private Window wind;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					GameWindow window = new GameWindow();
//					window.gameFrame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 * @param w 
	 */
	public GameWindow(String level, Window w) {
		this.level = level;
		wind = w;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		gameFrame = new JFrame();
		gameFrame.setTitle("One Weapon");
		gameFrame.setBounds(100, 100, App.screenX + 16, App.screenY + 38);
		gameFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		gameFrame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		gameFrame.setVisible(true);
		
		app = new App();
		app.wind = this.wind;
		gameFrame.add(app, BorderLayout.CENTER);
		app.setVisible(true);
		app.appStatic();
		app.openLevel(level);
		app.init();
		app.start();
	}
	
	public int play(String level) {
//		gameFrame.setVisible(true);
//		app.openLevel(Level.getLevel(level));
		int exitCode;
		do {
			exitCode = app.getExitCode();
			System.out.println("|	" + exitCode);
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while (exitCode == -1);
//		app.stop();
//		gameFrame.setVisible(false);
		return exitCode;
	}
}
