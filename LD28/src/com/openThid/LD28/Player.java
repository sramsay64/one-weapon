package com.openThid.LD28;

import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.util.HashMap;



@SuppressWarnings("javadoc")
public class Player extends Entity {
	
//	public static final float speed = (float) 0.1;
	
	@Deprecated
	public HashMap<String, Weapon> Weapons;
	
	public WeaponsBar weaponsBar;
	
	public int directionX = 0;
	public int directionY = 0;
	
	public Player() {
		this(32, 32, new WeaponsBar(App.screenX, 64));
	}
	
	public Player(WeaponsBar wb) {
		this(32, 32, wb);
	}
	
	public Player(int width, int height, WeaponsBar wp) {
		this(width, height, wp, getImage("res/player.png", App.app));
	}
	
	public Player(int width, int height, WeaponsBar wp, Image image) {
		weaponsBar = wp;
		this.height = height;
		this.width = width;
		this.image = image;
	}
	
	public void updatePos() {
		velX = directionX;
		velY = directionY;
		x += velX;
		y += velY;
	}
	
	public static Image getImage(String file, Object o) {
		URL url = o.getClass().getResource(file);
		Toolkit tk = Toolkit.getDefaultToolkit();
		return tk.getImage(url);
	}
	
	public void use(int mouseX, int mouseY, Enemy[] enemys) {
		weaponsBar.getSelectedWeapon().use(mouseX, mouseY, this, enemys);
		if (weaponsBar.getSelectedWeapon().getClass().getSimpleName().equals("Sword")) {
//			System.out.println("Sword");
		} else if (weaponsBar.getSelectedWeapon().getClass().getSimpleName().equals("Dart")) {
//			System.out.println("Dart");
		} else if (weaponsBar.getSelectedWeapon().getClass().getSimpleName().equals("else")) {
			System.out.println("else");
		} else {
			System.out.println("@" + weaponsBar.getSelectedWeapon().getClass().getSimpleName());
		}
	}
}